/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;
import java.util.Scanner;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
        int max = 13;
        int min =1;
        int range = max - min + 1;
        
        Card[] magicHand = new Card[7];
        
        for (int i=0; i<magicHand.length; i++)
        
        {
            int random0To3 = (int)Math.floor(Math.random() * 3);
            Card c = new Card();
            c.setValue((int)(Math.random() * range) + min);
            c.setSuit(Card.SUITS[random0To3]);
            magicHand[i] = c;
            
        }
     
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your card Value");
        int playerValue = input.nextInt();
        System.out.println("Please enter your suit");
        String playerSuit = input.next();
        
        Card playerCard = new Card();
        playerCard.setValue(playerValue); 
        playerCard.setSuit(playerSuit);
        boolean isCardFound = false;
        
        for(int i = 0; i < magicHand.length; i++){
            if(magicHand[i] == playerCard){
                isCardFound = true;
                break;
            }
          }
        
        if(isCardFound == true){
            System.out.println("you card: was in the magic hand");
        } else{
            System.out.println("Card was not in the magic Hand");
        }
        
        input.close();
        
    }
    
    
}
